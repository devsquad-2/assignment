import java.util.Arrays;

public class Problem_12 {

	public static void main(String[] args) {
		
		int array[] = {1,2,3,4};
		int tempVar;
		
		// Method #1
		Arrays.sort(array); 
		
		
//		// Method #2
//		for (int i = 0; i < array.length - 1; i++)
//		{
//		  for(int j = 0; j < array.length - 1; j++)
//		  {
//		    if(array[i] < array[j + 1])
//		    {
//		      tempVar = array [j + 1];
//		      array [j + 1]= array [i];
//		      array [i] = tempVar;
//		    }
//		  }
//		} 
		
		System.out.println("Second Largest : "+array[array.length-2]);
		System.out.println("Second Smallest : "+array[1]);

	}

}
