
public class Problem_16 {

	public static void main(String[] args) {
		int Num1=0,Num2=1,Num3;
		int Length = 9; // This is the Given Number
		int i = 1;
		
		
//		// Method #1 - Using for Loop
//		System.out.print(Num1+" "+Num2);
//		for(int Print = 2; Print < Length; ++Print){
//			Num3 = Num1 + Num2;    
//			System.out.print(" "+Num3);    
//			Num1 = Num2;    
//			Num2 = Num3;    
//		} 
		
		// Method #2 - Using While Loop
		while (i <= Length){
			System.out.print(Num1 + " ");
            Num3 = Num1 + Num2;
            Num1 = Num2;
            Num2 = Num3;
            i++;
        }
	}
}
