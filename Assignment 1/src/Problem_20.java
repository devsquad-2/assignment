
public class Problem_20 {
	public static void main(String[] args) {
		
		//Method #1 - Using Reverse
		String string = "Madam";
		String reverseStr = "";
		int strLength = string.length();

		for (int i = (strLength - 1); i >=0; --i) {
			reverseStr = reverseStr + string.charAt(i);
		}
		if (string.toLowerCase().equals(reverseStr.toLowerCase())) {
  			System.out.println(string + " is a Palindrome. --> Using Reverse");
		}else {
		    System.out.println(string + " is not a Palindrome. --> Using Reverse");
	    }
		
		
		//Method #2 - Using Without Reverse
		String input= "Program";
		char[] Char = input.toCharArray();
        String rvrseInput = "";
		for (int itration = (Char.length-1) ; itration >= 0; itration--) {
			rvrseInput = rvrseInput + Char[itration];
		}
		if(input.equals(rvrseInput)) {
			System.out.println(input+" is palindrome. --> Without Reverse");
		}else {
			System.out.println(input+" is not palindrome. --> Without Reverse");
	    }

	}

}
