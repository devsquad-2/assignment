import java.util.Scanner;

public class Problem_15 {

	public static void main(String[] args) {
		
		int[] Product_ID = new int[20];
		String[] Product_Name = new String[20];
		double[] Product_Price = new double[20];
		int[] Product_Quantity = new int[20];
		int Add = 0;
		boolean wish = true;
		
		double totalbill = 0;
		Scanner input = new Scanner(System.in);
		while(wish == true) {
			System.out.println("Enter Product ID: ");
			Product_ID[Add] = input.nextInt();
			input.nextLine();
			System.out.println("Enter Product Name: ");
			Product_Name[Add] = input.nextLine();
			System.out.println("Price in SDG : ");
			Product_Price[Add] = input.nextDouble();
			System.out.println("Quantity : ");
			Product_Quantity[Add] = input.nextInt();
			Add++;
			System.out.print("Wish to Continue: (Y/N) : ");
			String Inp = input.next();
			Inp = Inp.toLowerCase();
			if(Inp.equals("y")){
				wish = true;
			}else {
				wish = false;
			}	
		}
		System.out.println("--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*");
		System.out.println("Product ID		Product Name		Price in SGD		Quantity");
		for(int i = 0 ; i < Add ; i++) {
			System.out.println(Product_ID[i]+"			"+Product_Name[i]+"			"+Product_Price[i]+"			"+Product_Quantity[i]);
		}
		
		for(int Sum = 0 ; Sum < Add; Sum++) {
			totalbill += (Product_Price[Sum] * Product_Quantity[Sum]) ;
		}
		int discount = 20;
		double amount_discounted = (totalbill/100)*discount;
		double AmountToPay = totalbill - amount_discounted;
		System.out.println();
		System.out.println("--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*");
		System.out.println("Total Price		"+totalbill);
		System.out.println("Flat Discount		"+discount+"%");
		System.out.println("Discount amount		"+amount_discounted);
		System.out.println("Amount to Pay	   	"+AmountToPay);
		

	}

}
