import java.util.Arrays;

public class Problem_11 {

	public static void main(String[] args) {
		
		int unFilter[] = {1,2,3,1,2,3,4};
		
		int Length = unFilter.length;
		
		//Sort the array
		Arrays.sort(unFilter);
		
		int Filter = 0;
		int[] temp = new int[Length]; 
		
		// Compare and Remove
        for (int Compare=0; Compare < Length-1; Compare++){  
            if (unFilter[Compare] != unFilter[Compare+1]){  
            	temp[Filter++] = unFilter[Compare]; 
            }  
        } 
        
        // Print 
        temp[Filter++] = unFilter[Length-1]; 
        for (int i = 0; i < Filter; i++) {  
            System.out.print(temp[i]+" ");  
     }  
	}

}
