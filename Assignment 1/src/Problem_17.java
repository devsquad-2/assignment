import java.math.BigInteger;

public class Problem_17 {
	
	//Method #3 - BigInteger Method
	public static BigInteger fact(int N)
    {
        BigInteger Product = new BigInteger("1");
        for (int i = 2; i <= N; i++)
        	Product = Product.multiply(BigInteger.valueOf(i));
        return Product;
    }
	
	//Method #4 - Recursion Method
	static int factorial(int aNum){
		if (aNum == 0) {   
		    return 1;    
		}else {   
		    return(aNum * factorial(aNum-1)); 
		    }
		 }    

	public static void main(String[] args) {
		
		int Product;
		int factorial = 1;  
		int Num = 6;
		//Method #1 - For Loop
		for(Product = 1; Product <= Num; Product++){    
			factorial = factorial * Product;    
		 }    
		System.out.println("M #1 -- Factorial of "+Num+" is: "+factorial); 
		
		//Method #2 - While Loop
		while(Product <= Num) {
	         factorial = factorial * Product;
	         factorial++;
	      }
	      System.out.println("M #2 -- Factorial of "+Num+" is: "+factorial);
	    
	    //Method #3 - BigInteger Method
	    System.out.println("M #3 -- Factorial of "+Num+" is: "+fact(Num)); 
	    
		//Method #4 - Recursion Method
		factorial = factorial(Num);   
		System.out.println("M #4 -- Factorial of "+Num+" is: "+factorial);   

	}

}
