
public class Problem_19 {

	public static void main(String[] args) {
		
		//Method #2 - Using For Loop
		String string = "11221";
		String reverseStr = "";
		int strLength = string.length();

		for (int i = (strLength - 1); i >=0; --i) {
			reverseStr = reverseStr + string.charAt(i);
		}
		if (string.toLowerCase().equals(reverseStr.toLowerCase())) {
			System.out.println(string + " is a Palindrome.");
		}else {
		    System.out.println(string + " is not a Palindrome.");
		}
		
		//Method #1 - Using While Loop
		int Num = 13531, RvsNum = 0, remainder;
	    int ONum = Num;
	    while (Num != 0) {
	      remainder = Num % 10;
	      RvsNum = RvsNum * 10 + remainder;
	      Num /= 10;
	    }
	    if (ONum == RvsNum) {
	      System.out.println(ONum + " is Palindrome.");
	    }else {
	      System.out.println(ONum + " is not Palindrome.");
	    }
	}

}
